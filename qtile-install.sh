#!/bin/bash

#Update & Upgrade
sudo pacman -Syu --noconfirm &&

#Install aplications

sudo pacman -S zsh --noconfirm &&
sudo pacman -S curl --noconfirm &&
sudo pacman -S wget --noconfirm &&
sudo pacman -S gparted --noconfirm &&
sudo pacman -S jupyter-notebook --noconfirm &&
sudo pacman -S libreoffice  --noconfirm &&
sudo pacman -S qtile --noconfirm &&
sudo pacman -S rustup --noconfirm &&
sudo pacman -S cmake --noconfirm &&
sudo pacman -S  freetype2 --noconfirm &&
sudo pacman -S  fontconfig --noconfirm &&
sudo pacman -S  pkg-config --noconfirm &&
sudo pacman -S  make --noconfirm &&
sudo pacman -S  libxcb --noconfirm &&
sudo pacman -S  libxkbcommon --noconfirm &&
sudo pacman -S  python --noconfirm &&


#Clone Wallpaper's
git clone https://gitlab.com/garuda-linux/themes-and-settings/artwork/garuda-wallpapers.git &&

#Clone & Install Fonts Powerline
git clone https://github.com/powerline/fonts.git --depth=1 &&
cd fonts/ &&
./install.sh &&
cd .. &&
rm -rf fonts &&

#Clone repository & copy config

git clone https://github.com/antoniosarosi/dotfiles.git &&
cp -r dotfiles/.config/qtile ~/.config &&

#Install Ohmyzsh
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

